import { RegistroComponent } from './registro/registro.component';
import { LoginComponent } from './login/login.component';
import { TableComponent } from './body/table/table.component';
import { RouterModule, Routes } from '@angular/router';
import { LateralComponent } from './lateral/lateral.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

const APP_ROUTES: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'lateral', component: LateralComponent },
  { path: 'table', component: TableComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'table' }

];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash: true} );
