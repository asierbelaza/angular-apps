import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Componentes

import { TableComponent } from './body/table/table.component';
import { AppComponent } from './app.component';
import { LateralComponent } from './lateral/lateral.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RegistroComponent } from './registro/registro.component';
import { LoginComponent } from './login/login.component';
import { APP_ROUTING } from './app-routing.module';

// Servicios

@NgModule({
  declarations: [
    AppComponent,
    LateralComponent,
    HeaderComponent,
    FooterComponent,
    TableComponent,
    RegistroComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
