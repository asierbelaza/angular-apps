 $('.indexchart1').easyPieChart({
            easing: 'easeOutBounce',
            barColor : '#3fda45',
            lineWidth: 5,
            animate: 1000,
            size:80,
            scaleColor: false,
            lineCap: 'square',
            trackColor: '#e5e5e5',
            onStep: function(from, to, percent) {
                $(this.el).find('.w_percent').text(Math.round(percent));
            }
        }); 

 $('.indexchart2').easyPieChart({
            easing: 'easeOutBounce',
            barColor : '#fb3377',
            lineWidth: 5,
            animate: 1000,
            size:80,
            scaleColor: false,
            lineCap: 'square',
            trackColor: '#e5e5e5',
            onStep: function(from, to, percent) {
                $(this.el).find('.w_percent').text(Math.round(percent));
            }
        }); 

 $('.indexchart3').easyPieChart({
            easing: 'easeOutBounce',
            barColor : '#0383c5',
            lineWidth: 5,
            animate: 1000,
            size:80,
            scaleColor: false,
            lineCap: 'square',
            trackColor: '#e5e5e5',
            onStep: function(from, to, percent) {
                $(this.el).find('.w_percent').text(Math.round(percent));
            }
        }); 

 $('.indexchart4').easyPieChart({
            easing: 'easeOutBounce',
            barColor : '#c62be0',
            lineWidth: 5,
            animate: 1000,
            size:80,
            scaleColor: false,
            lineCap: 'square',
            trackColor: '#e5e5e5',
            onStep: function(from, to, percent) {
                $(this.el).find('.w_percent').text(Math.round(percent));
            }
        }); 